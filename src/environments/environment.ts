// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyDQ8EFSrYmZBm2K9wglZjN1m9EnT8TpO50",
    authDomain: "projetomobile3-9cab5.firebaseapp.com",
    databaseURL: "https://projetomobile3-9cab5.firebaseio.com",
    projectId: "projetomobile3-9cab5",
    storageBucket: "projetomobile3-9cab5.appspot.com",
    messagingSenderId: "908645088723",
    appId: "1:908645088723:web:5760e518912897c8ed0326"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
