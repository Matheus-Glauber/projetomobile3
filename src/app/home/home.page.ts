import { Component } from '@angular/core';
import { CrudService } from '../services/crud.service';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  users: any;

  userName: String;
  passwordUser: string;

  constructor(private crudService: CrudService) {}

  ngOnInit() {
    this.crudService.read_Users().subscribe(data => {

      this.users = data.map(e => {
        return{
          id: e.payload.doc.id,
          Name: e.payload.doc.data()['Name'],
          Password: e.payload.doc.data()['Password'],

        };
      })
    });
  }

  RemoveRecord(rowID){
    this.crudService.delete_Users(rowID);
  }

  CreateRecord(){
    let record = {};
    record['Name'] = this.userName;
    record['Password'] = this.passwordUser;

    this.crudService.create_new_user(record).then(resp => {
      this.userName = "";
      this.passwordUser = "";
    }).catch(error =>{
      console.log(error);
    })
  }
}
