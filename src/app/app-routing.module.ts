import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)},
  {
    path: 'cadastro-de-usuario',
    loadChildren: () => import('./paginas/cadastro-de-usuario/cadastro-de-usuario.module').then( m => m.CadastroDeUsuarioPageModule)
  },
  {
    path: 'lista-de-usuarios',
    loadChildren: () => import('./paginas/lista-de-usuarios/lista-de-usuarios.module').then( m => m.ListaDeUsuariosPageModule)
  },
  {
    path: 'autenticar-usuario',
    loadChildren: () => import('./paginas/autenticar-usuario/autenticar-usuario.module').then( m => m.AutenticarUsuarioPageModule)
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
