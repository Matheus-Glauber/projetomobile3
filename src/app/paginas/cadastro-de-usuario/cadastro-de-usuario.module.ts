import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CadastroDeUsuarioPageRoutingModule } from './cadastro-de-usuario-routing.module';

import { CadastroDeUsuarioPage } from './cadastro-de-usuario.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CadastroDeUsuarioPageRoutingModule
  ],
  declarations: [CadastroDeUsuarioPage]
})
export class CadastroDeUsuarioPageModule {}
