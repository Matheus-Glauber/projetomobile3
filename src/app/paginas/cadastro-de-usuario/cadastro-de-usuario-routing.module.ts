import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CadastroDeUsuarioPage } from './cadastro-de-usuario.page';

const routes: Routes = [
  {
    path: '',
    component: CadastroDeUsuarioPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CadastroDeUsuarioPageRoutingModule {}
