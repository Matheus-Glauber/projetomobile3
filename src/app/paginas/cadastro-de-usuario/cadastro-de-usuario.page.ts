import { Component, OnInit } from '@angular/core';
import { CrudService } from 'src/app/services/crud.service';

@Component({
  selector: 'app-cadastro-de-usuario',
  templateUrl: './cadastro-de-usuario.page.html',
  styleUrls: ['./cadastro-de-usuario.page.scss'],
})
export class CadastroDeUsuarioPage implements OnInit {

  users: any;

  userName: String;
  passwordUser: string;

  constructor(private crudService: CrudService) { }

  ngOnInit() {
    this.crudService.read_Users().subscribe(data => {

      this.users = data.map(e => {
        return{
          id: e.payload.doc.id,
          Name: e.payload.doc.data()['Name'],
          Password: e.payload.doc.data()['Password'],

        };
      })
    });
  }

  CreateRecord(){
    let record = {};
    record['Name'] = this.userName;
    record['Password'] = this.passwordUser;

    this.crudService.create_new_user(record).then(resp => {
      this.userName = "";
      this.passwordUser = "";
    }).catch(error =>{
      console.log(error);
    })
  }

}
