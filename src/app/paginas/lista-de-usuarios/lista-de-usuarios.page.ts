import { Component, OnInit } from '@angular/core';
import { CrudService } from 'src/app/services/crud.service';

@Component({
  selector: 'app-lista-de-usuarios',
  templateUrl: './lista-de-usuarios.page.html',
  styleUrls: ['./lista-de-usuarios.page.scss'],
})
export class ListaDeUsuariosPage implements OnInit {

  users: any;

  userName: String;
  passwordUser: string;

  constructor(private crudService: CrudService) { }

  ngOnInit() {
    this.crudService.read_Users().subscribe(data => {

      this.users = data.map(e => {
        return{
          id: e.payload.doc.id,
          Name: e.payload.doc.data()['Name'],
          Password: e.payload.doc.data()['Password'],

        };
      })
    });
  }

  RemoveRecord(rowID){
    this.crudService.delete_Users(rowID);
  }

}
