import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AutenticarUsuarioPage } from './autenticar-usuario.page';

const routes: Routes = [
  {
    path: '',
    component: AutenticarUsuarioPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AutenticarUsuarioPageRoutingModule {}
