import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AutenticarUsuarioPageRoutingModule } from './autenticar-usuario-routing.module';

import { AutenticarUsuarioPage } from './autenticar-usuario.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AutenticarUsuarioPageRoutingModule
  ],
  declarations: [AutenticarUsuarioPage]
})
export class AutenticarUsuarioPageModule {}
