import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root'
})
export class CrudService {

  constructor(private firestone: AngularFirestore) { }

  read_Users(){
    return this.firestone.collection('Users').snapshotChanges();
  }

  delete_Users(record_id){
    this.firestone.doc('Users/' + record_id).delete();
  }

  create_new_user(record){
    return this.firestone.collection('Users').add(record);
  }

}
